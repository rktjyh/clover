## Clover
This repo consists of scripts to install a custom arch setup for super human productivity. The configurations are hardcoded as per my preference and no options are provided to choose from, since this is an automation script, not an arch installer. 

Fork it, Tweak it and make it your own ! You can find the dot files [here](https://gitlab.com/frappe42/clover/-/tree/main/.config).

Screenshot 01  |  Screenshot 02
:-------------------------:|:-------------------------:
![](assets/readme_01.png)  |  ![](assets/readme_02.png)

## Pre-installation

* Download the arch iso from [this](https://archlinux.org/download/) page and [verify](https://wiki.archlinux.org/title/Installation_guide#Verify_signature) the signature of your download.
* Prepare the [USB flash installation medium](https://wiki.archlinux.org/title/USB_flash_installation_medium). (Do not use [these](https://wiki.archlinux.org/title/USB_flash_installation_medium#Inadvisable_methods) methods.)
* Boot the live environment in UEFI mode. Disable Secure boot as Arch linux images do not support secure boot.
* Connect to the internet using one of following techniques :
    * Plug in an ethernet cable.
    * Authenticate to a wireless network using [iwctl](https://wiki.archlinux.org/title/Iwd#iwctl).
    * Connect to a mobile network with the [mmcli](https://wiki.archlinux.org/title/Mobile_broadband_modem#ModemManager) utility.

To check if you are connected to the internet. Run the command `ping archlinux.org`.

## Installation

Use the below command from a live environment shell to [install](https://gitlab.com/frappe42/clover/-/blob/main/install.sh) and [setup](https://gitlab.com/frappe42/clover/-/blob/main/setup.sh) vanilla arch for everyday use.

```
curl https://gitlab.com/frappe42/clover/-/raw/main/install.sh -o install.sh; sh install.sh
```

* You will be prompted a few times to specify the hostname, username, passwd etc.
* Once the installation is complete. The machine will automatically reboot into arch and you will see the login page.

## Post-installation

<details><summary>Change Appearance</summary>
<br>

* Using `lxappearance` app to tweak theme, icon, font, cursor etc.
* Using `lightdm-gtk-greeter-settings` app to tweak login greeter.

</details>

<details><summary>Setup Shell Style</summary>
<br>

The script installs [fish](https://fishshell.com/) shell by default. You can install [oh-my-fish](https://github.com/oh-my-fish/oh-my-fish) fish shell framework using below command :

```bash
curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install | fish
```

Use the below command from fish shell to install the [box fish](https://github.com/oh-my-fish/oh-my-fish/blob/master/docs/Themes.md/#boxfish) theme. You can find more themes [here](https://github.com/oh-my-fish/oh-my-fish/blob/master/docs/Themes.md/).

```bash
omf install boxfish
```

</details>

<details><summary>Setup Neovim Editor</summary>
<br>

Use [Neovim](https://neovim.io/) ? Try the below command to auto-install my setup that just works. 

```bash
curl https://gitlab.com/frappe42/clover/-/raw/main/editor.sh -o editor.sh; sh editor.sh; rm editor.sh
```

</details>

<details><summary> Installed Apps </summary>
<br>

You can check the comments [here](https://gitlab.com/frappe42/clover/-/blob/main/setup.sh#L86) to see what apps are installed by the script.

</details>

<details><summary> Terminal Support in File Manager </summary>
<br>

The script installs [PcManFM](https://wiki.lxde.org/en/PCManFM) file manager. To enable terminal support in it :

1. Open file manager by pressing `Shift + Alt + F`.
2. Click on `Edit > Preferences > Advanced`.
3. Type **terminator** in the `Terminal emulator` text field.
4. Close the `Preferences` dialog box.

Now, you can press `F4` to open the current directory of the file manager in a terminal.

</details>

<details><summary> Package manager for AUR </summary>
<br>

You may want to install package from AUR. Hence paru !

```bash
pacman -S rustup
rustup install stable
rustup default stable
git clone https://aur.archlinux.org/paru.git
cd paru; makepkg -si; cd ..; rm -rf paru
```
</details>

<details><summary> Setup for VMs </summary>
<br>

If you work with VMs, use below commands for a quick KVM setup.

`fish shell does not support $, use bash for below commands`

```bash
sudo pacman -S virt-manager qemu vde2 ebtables dnsmasq bridge-utils openbsd-netcat
sudo systemctl enable libvirtd.service
sudo systemctl start libvirtd.service
sudo sed -i 's/#unix_sock_group = "libvirt"/unix_sock_group = "libvirt"/' /etc/libvirt/libvirtd.conf
sudo sed -i 's/#unix_sock_ro_perms = "0777"/unix_sock_ro_perms = "0777"/' /etc/libvirt/libvirtd.conf
sudo usermod -a -G libvirt $(whoami)
newgrp libvirt
sudo systemctl restart libvirtd.service
```
</details>

## Support
You can reach out to me on telegram : `frappe42`

## Roadmap

- [x] Setup Arch with i3 wm.
- [x] Setup Fish Shell.
- [x] Setup Neovim.
- [x] Setup VMs.
- [ ] Setup auto-backup.

## Project status
This is a long-term passive project. I contribute to this project if and when I come across something useful.

## License
This project is licensed under the GNU Affero General Public License v3.0.
