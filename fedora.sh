#!/bin/bash
# This script sets up fedora minimal install for everyday use.

set -euo pipefail;

setDnf () {

# update dnf attributes.
echo 'fastestmirror=1' | sudo tee -a /etc/dnf/dnf.conf
echo 'max_parallel_downloads=10' | sudo tee -a /etc/dnf/dnf.conf
echo 'deltarpm=true' | sudo tee -a /etc/dnf/dnf.conf

sudo dnf -y update;
sudo dnf -y upgrade;

# add RPM fusion repos.
rpm -Uvh http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-"$(rpm -E %fedora)".noarch.rpm;
rpm -Uvh http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-"$(rpm -E %fedora)".noarch.rpm;

}

setUser () {

    # test $users
    # input username.
    echo "Specify username."
    read userName;

    # enable wheel group in sudoers.
    sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers

    # add user to wheel group.
    usermod -a -G wheel $userName;

    # create common directories.
    dnf install -y xdg-user-dirs;
    runuser -l $userName -c "xdg-user-dirs-update";

    # download dot files into their desired paths.
    repo="https://gitlab.com/frappe42/clover/-/raw/main";

    # lightdm
    mkdir -p /etc/lightdm; curl "$repo"/.config/lightdm/lightdm-gtk-greeter.conf -o /etc/lightdm/lightdm-gtk-greeter.conf;

    # i3
    mkdir -p /home/"$userName"/.config/i3; curl "$repo"/.config/i3/config -o /home/"$userName"/.config/i3/config;
    mkdir -p /home/"$userName"/Pictures; curl "$repo"/assets/wallpaper -o /home/"$userName"/Pictures/wallpaper;

    # i3blocks
    mkdir -p /home/"$userName"/.config/i3; curl "$repo"/.config/i3/i3blocks.conf -o /home/"$userName"/.config/i3/i3blocks.conf;

    # rofi
    mkdir -p /home/"$userName"/.config/rofi; curl "$repo"/.config/rofi/config.rasi -o /home/"$userName"/.config/rofi/config.rasi;

    # terminator
    mkdir -p /home/"$userName"/.config/terminator; curl "$repo"/.config/terminator/config -o /home/"$userName"/.config/terminator/config;

    # fish
    mkdir -p /home/"$userName"/.config/fish/functions; curl "$repo"/.config/fish/config.fish -o /home/"$userName"/.config/fish/config.fish;
    curl "$repo"/.config/fish/functions/fish_greeting.fish -o /home/"$userName"/.config/fish/functions/fish_greeting.fish;

    # Reset permissions
    chown -R $userName  /home/$userName/.config
    chown -R :$userName /home/$userName/.config
    chown -R $userName  /home/$userName/Pictures
    chown -R :$userName /home/$userName/Pictures

}

setGUI () {

apps=(

'xorg-x11-server-Xorg' # display server
'xprop'                # display utility
'xclip'                # clipboard grabber
'lightdm'              # display manager
'lightdm-gtk-greeter'  # display greeter
'i3-gaps'              # window manager
'i3blocks'             # status bar
'lxappearance'         # theme switcher
'dunst'                # notification daemon
'rofi'                 # app launcher
'feh'                  # desktop wallpaper
'adwaita-gtk2-theme'   # color theme
'paper-icon-theme'     # icon  theme
)

for apps in "${apps[@]}"; do
        sudo dnf -y install "$apps";
done

sudo systemctl enable lightdm;
sudo systemctl set-default graphical.target;

}

setFont () {

	sudo dnf -y install google-noto-sans-symbols2-fonts;

}

setAudio () {

    sudo dnf -y install pulseaudio alsa-plugins-pulseaudio pavucontrol;
}

setNetwork () {

sudo dnf -y install NetworkManager;
sudo systemctl enable NetworkManager;

}

setBluetooth () {

sudo dnf -y install blueman bluez;
sudo systemctl enable bluetooth;

}

setPower () {

sudo dnf -y install tlp tlp-rdw acpi brightnessctl;
sudo systemctl enable tlp;

}

setShell () {

sudo dnf -y install \
    xfce4-terminal \
    fish \
    neovim \
    util-linux-user;

# Fix hostname
# sed -i 's/hostname/uname -n/' /home/"$USER"/.config/fish/functions/fish_prompt.fish

# Install oh-my-fish
# runuser -l $userName -c "curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install | fish";

# Install theme for fish
# runuser -l $userName -c "omf install boxfish";

# Set fish as default shell
runuser -l $userName -c "chsh --shell /bin/fish "$userName"";

# Set neovim as deafult editor
echo "export VISUAL=nvim" | sudo tee -a /etc/profile;
echo "export EDITOR=nvim" | sudo tee -a /etc/profile;

}

setApps () {

apps=(

'pcmanfm' 		        # file manager
'gvfs-mtp'		        # mtp support
'tar' 			        # tar
'lbzip2'                # bzip2
'unzip' 		        # unzip
'android-tools' 	    # adb
'gedit' 		        # text editor
'evince' 		        # doc viewer
'ristretto' 		    # image viewer
'vlc' 			        # media player
'firefox' 		        # primary browser
'chromium' 		        # secondary browser
'torbrowser-launcher' 	# tor browser
'fragments' 		    # torrent client
'gnome-screenshot' 	    # screenshot tool
'galculator' 		    # calculator tool
'gnome-disk-utility' 	# disks app
'gnome-multi-writer' 	# iso writer
'audacity' 		        # sound recorder/editor
'gcolor3' 		        # color picker
'kolourpaint' 		    # drawing app
'mypaint' 		        # raster painting
'minder'                # mind maps
'peek' 			        # GIF recorder
'seahorse' 		        # encryption keys
'gimp' 			        # image manipulation app
'inkscape' 		        # vector based drawing
'graphviz' 		        # graph drawing app
'obs-studio' 		    # studio app
'pitivi' 		        # video editor

# cli apps

'htop' 			        # task manager
'exa' 			        # ls  alternative
'bat' 			        # cat alternative
'git'			        # version control
'pandoc' 		        # markup converter
'jq'			        # json processor
'acpi' 			        # battery client
'brightnessctl' 	    # brightness control
);

for app in "${apps[@]}"; do
        sudo dnf -y install "$app";
done

# only office
sudo yum -y install https://download.onlyoffice.com/repo/centos/main/noarch/onlyoffice-repo.noarch.rpm
sudo dnf -y install onlyoffice-desktopeditors

# RPMS
# pencil project ~ https://pencil.evolus.vn/
# draw io ~ https://www.diagrams.net/

}

# Update/Upgrade your system


setDnf;
setUser;
setGUI;
setFont;
setAudio;
setNetwork;
setBluetooth;
setPower;
setShell;
setApps;

sleep 10;
reboot;
